const http = require("http");

const port = 3000;

const server = http.createServer(function(request, response){
	if(request.url == "/"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Welcome to my page!");
	}

	else if(request.url == "/login"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Welcome to the login page.");
	}

	else{ // block of code if incase the endpoint or url does not match
		response.writeHead(404, {"Content-Type" : "text/plain"});
		response.end("I'm sorry, the page you are looking for cannot be found.");
	}


});

server.listen(port);
console.log(`Server is now accessible at localhost:${port}`);


/*	- What directive is used by Node.js in loading the modules it needs?

A: require
*/

/*	- What Node.js module contains a method for server creation?

A: http module
*/

/*	- What is the method of the http object responsible for creating a server using Node.js?

A: createServer()
*/

/*	- What method of the response object allows us to set status codes and content types?

A: writeHead()
*/

/*	- Where will console.log() output its contents when run in Node.js?

A: terminal
*/

/*	- What property of the request object contains the address's endpoint?


A: request.url
*/